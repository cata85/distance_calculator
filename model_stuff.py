import requests


def calculate_distance(extra_url):
    url = 'http://www.distance24.org/route.json?stops=' + str(extra_url)
    r = requests.get(url).json()
    return r['distance']
