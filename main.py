from flask import Flask, jsonify, render_template, request
import model_stuff


app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/distance', methods=['GET'])
def distance():
    extra_url_stuff = request.args.get('stops')
    x = model_stuff.calculate_distance(extra_url_stuff)
    return jsonify(x)


if __name__ == '__main__':
    app.run(debug=True)
