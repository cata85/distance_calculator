$(document).ready(function(){
    $('button').click(function(){

        var search_terms = {
            stops: $('#first_destination').val() + '|' + $('#second_destination').val()
        };
        distance(search_terms);
    });

    function distance(stops){
        $.ajax({
            url: '/distance',
            type: 'GET',
            data: stops,
            error: function (err) {
                $('#para').html('not a correct entry')
            },
            success: function(data){
                $('#para').html(data + ' miles');
            }
        });

    }
});